package br.com.construtora.bean;
import java.awt.List;

public class Construcao {
	
	public String finalidade;
	public Pessoa pessoa;
	public List portas;
	
	public String getFinalidade() {
		return finalidade;
	}
	public void setFinalidade(String finalidade) {
		this.finalidade = finalidade;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public List getPortas() {
		return portas;
	}
	public void setPortas(List portas) {
		this.portas = portas;
	}
	
}
