package br.com.construtora.bean;

public class Circulo {
	
	public Circulo(){
		Ponto ponto = new Ponto();
	};
	
	public Circulo(int raio, Ponto ponto) {
		Circulo ciculo = new Circulo(raio, ponto);
	};
	
	private int raio;
	private Ponto ponto;

	public int getRaio() {
		return raio;
	}

	public void setRaio(int raio) {
		this.raio = raio;
	}

	public Ponto getPonto() {
		return ponto;
	}

	public void setPonto(Ponto ponto) {
		this.ponto = ponto;
	}

}
