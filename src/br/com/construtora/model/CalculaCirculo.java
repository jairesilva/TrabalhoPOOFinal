package br.com.construtora.model;

import br.com.construtora.bean.Circulo;
import br.com.construtora.bean.Ponto;

public class CalculaCirculo {

	public Double calculaCircunferencia(){
		
		Circulo c = new Circulo();
		Ponto p = new Ponto();
		p.setX(8);
		p.setY(6);
		
		c.setRaio(10);
		c.setPonto(p);
		
	    int diametro = 2 * c.getRaio(); 
	    Double circunferencia = diametro * Math.PI;
	    
	    System.out.print( "\nDiametro : " + diametro );
	    System.out.print( "\nDiametro : " + circunferencia );
	    
	    return circunferencia;
	    
	}
	
	public Double calculaArea(){
		
		Circulo c = new Circulo();
		Ponto p = new Ponto();
		p.setX(16);
		p.setY(18);
		c.setRaio(27);
		c.setPonto(p);
		
	    int diametro = 2 * c.getRaio(); 
	    Double area = Math.pow(c.getRaio(), 2) * Math.PI;
	    
	    System.out.print( "\nDiametro : " + diametro );
	    System.out.print( "\nDiametro : " + area );
	    
	    return area;
	    
	}
}